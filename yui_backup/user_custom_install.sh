# !/bin/bash
#pacman -S zsh --noconfirm
#username=sharuru
#useradd -m -G wheel -s /bin/zsh $username
#passwd $username
## seh wheel group in sudoers file
#sed -i '/^#.%wheel/s/^# //' /etc/sudoers
#
## add archlinuxfr repo and activate multilib
#sed -i '/^#\[multilib\]/{N;s/#//;N;s/#//}' /etc/pacman.conf
#echo -e '\n[archlinuxfr]\nServer = http://repo.archlinux.fr/$arch\nSigLevel = Never' >> /etc/pacman.conf
#pacman -Syu
#pacman -S yaourt --noconfirm
#
## user config
#su $username
yaourt -Syua
yaourt -S oh-my-zsh-git --noconfirm
#cp /usr/share/oh-my-zsh/zshrc ~/.zshrc
#sed -i 's/robbyrussell/ys/' ~/.zshrc

# install graphic drivers & bumblebee
yaourt -S mesa xf86-video-intel  lib32-virtualgl lib32-mesa-libgl --noconfirm
yaourt -S xorg xorg-apps xorg-server xorg-xinit --noconfirm

sudo sh -c "echo -e 'Section\t\"Device\"\n\tIdentifier\t\"Intel Graphics\"\n\tDriver\t\"intel\"\n\tOption\t\"AccelMethod\"\t\"uxa\"\n\tOption\t\"TearFree\"\t\"true\"\n\tOption\t\"DRI\"\t\"3\"\nEndSection' > /etc/X11/xorg.conf.d/20-intel.conf"
#sudo localectl --no-convert set-x11-keymap

yaourt -S i3 i3blocks feh j4-dmenu-desktop lightdm lightdm-gtk-greeter compton nemo --noconfirm
sudo systemctl enable lightdm

# TODO : VA-API & VDPAU
# https://wiki.archlinux.org/index.php/VA-API
# https://wiki.archlinux.org/index.php/VDPAU


# desktop environment : gnome (for instance)
#yaourt -S gnome gdm file-roller --noconfirm
#sudo systemctl enable gdm

# arc theme
#yaourt -S gtk-theme-arc gnome-tweak-tool --noconfirm

# TODO: network stuff resolvconf.conf & dnsmasq conf
#yaourt -S networkmanager network-manager-applet networkmanager-openvpn dnsmasq --noconfirm
yaourt -S dnsmasq
# sudo systemctl enable NetworkManager.service

# download dnsmasq conf
#sudo systemctl enable dnsmasq.service

# TODO: ntp stuff

# TODO: touchpad

# tlp
yaourt -S tlp --noconfirm
sudo systemctl enable tlp.service
sudo systemctl enable tlp-sleep.service
sudo systemctl disable systemd-rfkill.service

# utils
yaourt -S ntfs-3g dosfstools exfat-utils gparted wget sl samba bind-tools htop net-tools alsa-utils zip unzip bluez bluez-utils --noconfirm
sudo systemctl enable fstrim.timer

# TODO: add cups music
yaourt -S sane cups foomatic-db-engine foomatic-db-ppds foomatic-db-nonfree-ppds foomatic-db-gutenprint-ppds --noconfirm
sudo systemctl start org.cups.cupsd.service
sudo systemctl enable org.cups.cupsd.service

# standard apps
yaourt -S google-chrome atom sublime-text-dev terminator p7zip unrar deluge ranger keychain slack-desktop discord atool file-roller --noconfirm
# standard fonts
yaourt -S ttf-inconsolata noto-fonts noto-fonts-cjk noto-fonts-emoji ttf-freefont ttf-arphic-uming ttf-baekmuk --noconfirm
# japanese fonts
yaourt -S adobe-source-han-sans-jp-fonts --noconfirm

# TODO: dev stuff android studio
# dev apps
yaourt -S jdk7-openjdk jdk8-openjdk openjdk7-doc openjdk8-doc git subversion intellij-idea-ultimate-edition mysql-workbench visualvm nodejs pycharm-community npm nvm --noconfirm

# android dev
#yaourt -S android-sdk android-sdk-platform-tools android-sdk-build-tools android-studio android-udev android-platform-22 --noconfirm

#sudo groupadd sdkusers
#sudo gpasswd -a $USERNAME sdkusers
#sudo chown -R :sdkusers /opt/android-sdk/
#sudo chmod -R g+w /opt/android-sdk/

echo -e "\nenter git user name:"
read git_username

echo -e "\nenter git user mail:"
read git_usermail

git config --global user.name  $git_username
git config --global user.email $git_usermail

# longcat
yaourt -S miniupnpc p7zip scrypt --noconfirm

# docker
yaourt -S docker --noconfirm
sudo systemctl start docker
sudo systemctl enable docker
sudo gpasswd -a ${USER} docker
newgrp docker

# TODO: virt stuff

sudo reboot
