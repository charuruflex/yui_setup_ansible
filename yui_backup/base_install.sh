# !/bin/bash
pacman -S vim --noconfirm

echo "setting up locale"
sed -i 's/#en_US.UTF-8 UTF-8/en_US.UTF-8 UTF-8/g' /etc/locale.gen
sed -i 's/#en_US ISO-8859-1/en_US ISO-8859-1/g' /etc/locale.gen
sed -i 's/#fr_FR.UTF-8 UTF-8/fr_FR.UTF-8 UTF-8/g' /etc/locale.gen
sed -i 's/#fr_FR ISO-8859-1/fr_FR ISO-8859-1/g' /etc/locale.gen
locale-gen
echo "LANG=en_US.UTF-8" > /etc/locale.conf

echo "set keyboard to fr"
echo -e "KEYMAP=fr\nFONT=lat9w-16" > /etc/vconsole.conf

echo "setting time"
ln -fs /usr/share/zoneinfo/Europe/Paris /etc/localtime
hwclock --systohc --utc

#echo "add nvme to modules before generate initramfs image"
#sed -i 's/MODULES=""/MODULES="nvme"/g' /etc/mkinitcpio.conf

#echo "if everything is good press any key to continue, Ctrl+C otherwise..."
#read
mkinitcpio -p linux

echo "installing bootloader"

pacman -S intel-ucode --noconfirm
bootctl install

#boot_entries=arch-new
#echo -e "default\t${boot_entries}\ntimeout\t3" > /boot/loader/loader.conf
#root_partition=`grep "/dev/" /etc/fstab | head -n1 | cut -d ' ' -f 2`
#echo -e "title\t\tArch Linux fresh install\nlinux\t\t/vmlinuz-linux\ninitrd\t\t/intel-ucode.img\ninitrd\t\t/initramfs-linux.img\noptions\t\troot=PARTUUID=`blkid -s PARTUUID -o value $root_partition` rw" > /boot/loader/entries/${boot_entries}.conf

echo "setting hostname, enter a name:"
read hostname
echo $hostname > /etc/hostname
sed -i "s/localhost$/localhost    $hostname/" /etc/hosts

# setup network (wifi/ethernet dhcp)
pacman -S wpa_supplicant dialog ifplugd wpa_actiond --noconfirm

cp /etc/netctl/example/ethernet-dhcp /etc/netctl/


# setup openssh and enable service
pacman -S openssh --noconfirm
systemctl enable sshd

echo "enter root password:"
passwd

echo "authorizing root login"
sed -i 's/#PermitRootLogin prohibit-password/PermitRootLogin on/g' /etc/ssh/sshd_config
