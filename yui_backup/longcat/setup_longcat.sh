#!/bin/sh

longcat_archive="longcat.bz2"

# Get the latest version of longcat
curl -u xxx:xxx https://longcat.sharuruzure.fr/longcat -o $longcat_archive

tar xvjf $longcat_archive

cd longcat*
sudo ./install-node --load-backup sharuruzure_yui.longcat-backup
