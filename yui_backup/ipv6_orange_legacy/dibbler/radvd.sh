#!/bin/bash
if [ $SRV_MESSAGE = "REPLY" ]
then
#!/bin/bash
LAN=lan1
WAN=orange
taille=${#PREFIX1}
taille=$((taille-4))

cat > /etc/radvd.conf << EOF
interface ${LAN}
{ 
     AdvSendAdvert on; 
     prefix ${PREFIX1:0:taille}10::/64
     { 

         AdvOnLink on;
         AdvPreferredLifetime 86400;
         AdvValidLifetime 86400;
         AdvAutonomous on;
   #	 AdvRouterAddr on;
     };
RDNSS ${PREFIX1:0:taille}10::1
        {
                AdvRDNSSLifetime 30;
        };
};
EOF

mv /etc/ip6deconf-new.sh /etc/ip6deconf-old.sh
cat > /etc/ip6conf.sh << EOF
/etc/ip6deconf-old.sh
ip -6 route add fe80::ba0:bab dev ${WAN}
ip -6 route add default via fe80::ba0:bab dev ${WAN}
ip -6 route add ${PREFIX1:0:taille}10::/64 dev ${LAN}
ip -6 addr add ${PREFIX1:0:taille}10::1/64 dev ${LAN}
systemctl restart radvd ##J'utilise systemd pour gérer mes démons et donc pour redemarrer radvd
EOF
cat > /etc/ip6deconf-new.sh << EOF
ip -6 route del fe80::ba0:bab dev ${WAN}
ip -6 route del default via fe80::ba0:bab dev ${WAN}
ip -6 route del ${PREFIX1:0:taille}10:: dev ${LAN}
ip -6 addr del ${PREFIX1:0:taille}10::1/64 dev ${LAN}
EOF
chmod +x /etc/ip6conf.sh
chmod +x /etc/ip6deconf-new.sh
/etc/ip6conf.sh
else
echo "Warning error" > /etc/dibbler/radvd.cfg
fi
