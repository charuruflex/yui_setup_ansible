# !/bin/bash
echo -e "setting keyboard to fr\n\n"
loadkeys fr

# conf network (wifi)
#echo -e "\nconfiguring wifi connection"
#wifi-menu

# setting password for distant ssh access
#passwd

# start sshd
#echo -e "\nstarting sshd.service"
#systemctl start sshd

# conf partition
echo -e "I/ Partitioning\n\n"

while [[ $need_partitioning != "y" && $need_partitioning != "n" ]]
do
    echo -e "Need to partition disk? (y/n)\n\n"
    read need_partitioning
done

if [[ $need_partitioning == "y" ]]
then
    
fi
echo -e "\ndisplaying disks on system:\n"
  lsblk
echo -e "\nwhich disk do you want to use?"
read disk
while [[ $correct_disk != "yes" && $correct_disk != "no" ]]
do
echo -e "\nis $disk correct? (yes/no)\n"
read correct_disk
done;
if [[ $is_correct_disk = "no" ]]; then echo -e "\nexiting..."; exit 0; fi;
done;
echo "wiki : https://wiki.archlinux.org/index.php/Beginners%27_guide#Create_new_partition_table"
parted $disk

echo -e "\nchoosing partitions:\n"
lsblk $disk
echo -e "\nwhich partition for /?"
read root_part
echo -e "\nwhich partition for /boot?"
read boot_part
echo -e "\nwhich partition for swap?"
read swap_part
echo -e "\nusing $root_part for /"
echo "using $boot_part for /boot"
echo "using $swap_part for swap"
while [[ $partition_ok != "yes" && $partition_ok != "no" ]]
do
echo "ok with theses? (yes/no)"
read partition_ok
done;
if [[ $partition_ok = "no" ]]; then echo -e "\nexiting..."; exit 0; fi;

echo -e "\nformating and mounting partitions:\n"
echo "$root_part will be formated to ext4 (mkfs.ext4)"
while [[ $format_boot_ok != "yes" && $format_boot_ok != "no" ]]
do
echo "need to format $boot_part? (yes/no)"
read format_boot_ok
done;
if [[ $format_boot_ok = "yes" ]]; then echo "$boot_part will be formated to fat32 (mkfs.fat -F32)"; fi;
echo "$swap_part will be formated and used as swap (mkswap)"
while [[ $formating_ok != "yes" && $formating_ok != "no" ]]
do
echo "is everything ok? (yes/no)"
read formating_ok
done;
if [[ $formating_ok = "no" ]]; then echo -e "\nexiting..."; exit 0; fi;
mkfs.ext4 $root_part
if [[ $format_boot_ok = "yes" ]]; then mkfs.fat -F32 $boot_part; fi;
mkswap $swap_part
swapon $swap_part

mount $root_part /mnt
mkdir -p /mnt/boot
mount $boot_part /mnt/boot

# installation
echo -e "\ninstalling base packages\n"
pacstrap /mnt base base-devel
echo -e "\ninit fstab"
genfstab -U /mnt >> /mnt/etc/fstab
nano /mnt/etc/fstab
echo -e "\nchange root to new system"
arch-chroot /mnt /bin/bash
